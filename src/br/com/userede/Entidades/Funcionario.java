package br.com.userede.Entidades;

public class Funcionario extends Pessoa {
    // atributos

    private long funcional;

    // construtores

    public Funcionario() {
    }

    public Funcionario(String nome, String cpf, String telefone, long funcional) {
        this.funcional = funcional;
        setNome(nome);
        setCpf(cpf);
        setTelefone(telefone);


    }
// geters and seters

    public long getFuncional() {
        return funcional;
    }

    public void setFuncional(long funcional) {
        this.funcional = funcional;
    }

    @Override
    public String toString() {
        StringBuilder retorno = new StringBuilder();

        retorno.append("Nome: " + getNome() + "\n");
        retorno.append("CPF: " + getCpf() + "\n");
        retorno.append("Telefone: " + getTelefone() + "\n");
        retorno.append("Funcional: " + this.funcional + "\n");

        return retorno.toString();

    }


}

