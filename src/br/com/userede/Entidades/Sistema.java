package br.com.userede.Entidades;

import javax.swing.*;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class Sistema {

    // método que solicita números inteiros já fazendo o tratamento de excessão

    public static int solicitarNumInteiro(String frase) {
        int num;
        try {
            System.out.println(frase);
            num = Io.entrada().nextInt();

        } catch (InputMismatchException e) {
            System.out.println("Digite apenas números inteiros: ");
            num = Io.entrada().nextInt();
        }
        return num;
    }

    // método que solicita números reais e trata a excessão

    public static double solicitarNumReal(String frase) {
        double num;
        try {
            System.out.println(frase);
            num = Io.entrada().nextDouble();

        } catch (InputMismatchException e) {
            System.out.println("Digite apenas números: ");
            num = Io.entrada().nextDouble();
        }
        return num;
    }

    // cadastrando um imóvel
    public static Imovel cadastrarImovel() {
        // endereço
        System.out.println("cadastre o endereço do imóvel: ");
        System.out.println("Rua: ");
        String rua = Io.entrada().nextLine();

        int numRua = solicitarNumInteiro("Número: ");
        System.out.println("CEP: ");
        String cep = Io.entrada().nextLine();
        EnderecoImovel objEndereco = new EnderecoImovel(rua, numRua, cep);
        // funcionário responsável pelo imóvel
        System.out.println("Cadastre o funcionário responsavel pelo imóvel: ");
        System.out.println("Nome: ");
        String nome = Io.entrada().nextLine();
        System.out.println("CPF: ");
        String cpf = Io.entrada().nextLine();
        System.out.println("Telefone: ");
        String telefone = Io.entrada().nextLine();

        long funcional = solicitarNumInteiro("Funcional: ");
        Funcionario objFuncionario = new Funcionario(nome, cpf, telefone, funcional);
        // valor do aluguel

        double valorAluguel = solicitarNumReal("Valor do aluguel: R$ ");

        Imovel objImovel = new Imovel(cadastrarMorador(), objEndereco, objFuncionario, valorAluguel);
        Imobiliaria.adicionarImovel(objImovel);
        return objImovel;
    }

    // cadastrando moradores
    public static List<Morador> cadastrarMorador() {
        List<Morador> listaDeMoradores = new ArrayList<>();

        int contador = 1;

        int qtdMoradores = solicitarNumInteiro("Digite a quantidade de moradores: ");
        while (contador <= qtdMoradores) {

            System.out.println("Nome: ");
            String nomeMorador = Io.entrada().nextLine();
            System.out.println("Telefone: ");
            String telefoneMorador = Io.entrada().nextLine();

            System.out.println("CPF: ");
            String cpfMorador = Io.entrada().nextLine();
// verificando se já existe um morador com o mesmo CPF no mesmo imóvel
            for (Morador elementoLista : listaDeMoradores) {
                if (elementoLista.getCpf().equals(cpfMorador)) {

                    System.out.println("Já existe um morador com esse CPF");
                    System.out.println("Verifique os dados do morador e tente novamente:");
                    cadastrarMorador();

                }
            }

            listaDeMoradores.add(Imobiliaria.verificarCpf(new Morador(nomeMorador, cpfMorador, telefoneMorador)));

            contador++;

        }
        return listaDeMoradores;
    }

    // método pra executar o sistema
    public static void executarSistema() {
        Sistema.menu();
    }

    // menu
    public static void menu() {
        int opcaoMenu = 0;
        System.out.println("Bem-vindo(a) ao sistema da imobiliária !");
        do {
            System.out.println("Escolha uma das opções abaixo: ");
            System.out.println("'1' para cadastrar um imóvel; ");
            System.out.println("'2' para excluir um morador de um imóvel; ");
            System.out.println("'3' para exibir a lista de imóveis; ");
            System.out.println("'4' para encerrar o sistema. ");
            opcaoMenu = Io.entrada().nextInt();

            switch (opcaoMenu) {
                case 1:
                    cadastrarImovel();
                    break;
                case 2:
                    Sistema.apagarMorador();
                    break;
                case 3:
                    Imobiliaria.mostrarImoveis();
                    break;
                case 4:
                    System.out.println("Sistema encerrado !");
                    break;
                default:
                    System.out.println("Opção inválida ");

            }
        } while (opcaoMenu != 4);

    }

    // método para apagar um morador específico de um imóvel parte 1
    // recebe o cep, número do imóvel e o cpf do morador que vai ser apagado
    // chama os métodos para apagar um morador parte 2 e parte 3 passando como parâmetro as informaçoes recebidas do endereço e cpf do morador
    public static void apagarMorador() {
        System.out.println("Primeiro, digite o endereço do imóvel: ");
        System.out.println("CEP: ");
        String cep = Io.entrada().nextLine();
        System.out.println("Número: ");
        int numero = Io.entrada().nextInt();
        System.out.println("Agora, digite o CPF do morador que deseja apagar: ");
        String cpf = Io.entrada().nextLine();

        Imovel.apagarMorador(Imobiliaria.pesquisarEndereco(cep, numero), cpf);
    }

}

