package br.com.userede.Entidades;

import java.util.ArrayList;
import java.util.List;

public class Imobiliaria {
    // atributos
    private static List<Imovel> listaDeImoveis = new ArrayList<>();

    // geter and seter
    public static List<Imovel> getListaDeImoveis() {
        return listaDeImoveis;
    }

    public static void setListaDeImoveis(List<Imovel> listaDeImoveis) {
        Imobiliaria.listaDeImoveis = listaDeImoveis;
    }


    // método que adiciona um imóvel na lista
    public static void adicionarImovel(Imovel novoImovel) {
        listaDeImoveis.add(novoImovel);
    }

    // método que mostra a lista de imóveis
    public static void mostrarImoveis() {
        System.out.println("Lista de imóveis cadastrados na imobiliária: ");
        if (listaDeImoveis.size() == 0) {
            System.out.println("Lista vázia, ainda não foi cadastrado nenhum imóvel ");
        } else {
            for (Imovel elementoLista : listaDeImoveis) {
                System.out.println(elementoLista);
            }
        }
    }

    // método para apagar morador parte 2
    // recebe o cep e o número do imóvel do morador que vai ser apagado e retorna o imovel
    public static Imovel pesquisarEndereco(String cep, int numero) {
        for (Imovel elementoLista : listaDeImoveis) {
            if (elementoLista.getEndereco().getCep().equals(cep)) {
                if (elementoLista.getEndereco().getNumero() == numero) {
                    return elementoLista;
                }
            }
        }
        return null;
    }
// verificando cpf dos moradores de todos imóveis

    public static Morador verificarCpf(Morador morador) {
        for (Imovel elementoListaImovel : listaDeImoveis) {
            for (Morador elementoListaMorador : elementoListaImovel.getListaDeMoradores()) {
                if (elementoListaMorador.getCpf().equals(morador.getCpf())) {
                    System.out.println("Já existe um morador com esse CPF");
                    System.out.println("Verifique os dados do morador e tente novamente: ");
                    Sistema.cadastrarMorador();
                }
            }
        }
        return morador;
    }
}

