package br.com.userede.Entidades;

public class EnderecoImovel {
    // atributos
    private String rua;
    private int numero;
    private String cep;

    // construtores

    public EnderecoImovel() {
    }

    public EnderecoImovel(String rua, int numero, String cep) {
        this.rua = rua;
        this.numero = numero;
        this.cep = cep;
    }
// geters and seters


    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    @Override
    public String toString() {
        StringBuilder retorno = new StringBuilder();
        retorno.append("Rua: " + this.rua + "\n");
        retorno.append("Número: " + this.numero + "\n");
        retorno.append("CEP: " + this.cep + "\n");
        return retorno.toString();
    }
}

