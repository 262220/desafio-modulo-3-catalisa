package br.com.userede.Entidades;

import java.util.ArrayList;
import java.util.List;

public class Imovel {
    // atributos
    private static List<Morador> listaDeMoradores = new ArrayList<>();
    private EnderecoImovel endereco;
    private Funcionario funcionario;
    private double valorAluguel;

    // construtores

    public Imovel() {
    }

    public Imovel(List<Morador> listaDeMoradores, EnderecoImovel endereco, Funcionario funcionario, double valorAluguel) {
        this.listaDeMoradores = listaDeMoradores;
        this.endereco = endereco;
        this.funcionario = funcionario;
        this.valorAluguel = valorAluguel;
    }
// geters and seters

    public double getValorAluguel() {
        return valorAluguel;
    }

    public void setValorAluguel(double valorAluguel) {
        this.valorAluguel = valorAluguel;
    }

    public EnderecoImovel getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoImovel endereco) {
        this.endereco = endereco;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public static List<Morador> getListaDeMoradores() {
        return listaDeMoradores;
    }

    public static void setListaDeMoradores(List<Morador> listaDeMoradores) {
        Imovel.listaDeMoradores = listaDeMoradores;
    }

    // método que apaga um morador parte 3
    // recebe o imóvel e o cpf do morador que vai ser apagado
    public static void apagarMorador(Imovel imovel, String cpf) {
        Morador moradorApagado = null;
        for (Morador elementoLista : imovel.listaDeMoradores) {
            if (elementoLista.getCpf().equals(cpf)) {
                moradorApagado = elementoLista;
                System.out.println("Morador apagado com sucesso!");
            } else {
                System.out.println("Erro, endereço ou CPF do morador não encontrado !");
            }
        }
        imovel.listaDeMoradores.remove(moradorApagado);
    }

    @Override
    public String toString() {
        StringBuilder retorno = new StringBuilder();
        retorno.append("-------------------------------------------------- \n");
        retorno.append("\n Endereço do Imóvel: \n");
        retorno.append(endereco);
        retorno.append("Valor do aluguel: " + valorAluguel + "\n");
        retorno.append("-------------------------------------------------- \n");
        retorno.append("Funcionário responsável: \n");
        retorno.append(funcionario + "\n");
        retorno.append("-------------------------------------------------- \n");
        retorno.append("\n Morador(es): \n");
        retorno.append(listaDeMoradores + "\n");
        retorno.append("-------------------------------------------------- \n");
        return retorno.toString();
    }
}



