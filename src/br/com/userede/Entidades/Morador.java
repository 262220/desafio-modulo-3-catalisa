package br.com.userede.Entidades;

public class Morador extends Pessoa {

    // construtores

    public Morador() {
    }

    public Morador(String nome, String cpf, String telefone) {
        setNome(nome);
        setCpf(cpf);
        setTelefone(telefone);
    }

    @Override
    public String toString() {
        StringBuilder retorno = new StringBuilder();
        retorno.append("Nome: " + getNome() + " \\ ");
        retorno.append("CPF: " + getCpf() + " \\ ");
        retorno.append("Telefone: " + getTelefone() + " \\ \n");
        return retorno.toString();

    }

}

